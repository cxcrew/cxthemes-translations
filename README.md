# CXThemes Translations

Here you will find files of translations for the plugins by CXThemes including:

- CXThemes WooCommerce Email Customizer
- CXThemes WooCommerce Create Customer on Order
- CXThemes WooCommerce Save and Share Cart
- CXThemes WooCommerce Shop as Customer

[You can buy and download these plugins on CodeCanyon here.](https://codecanyon.net/user/cxthemes/portfolio)

Please feel free to edit and contribute to these translations. We will accept legitimate pull requests.
